# Contributing to Pilot

Hello there! If you are reading this, you likely want to contribute to the Pilot project with an issue or merge request. Do note that there are guidelines and rules that you should follow when contributing to this repository to keep the project well-maintained, and that contributions may end up taking quite a while due to the heavy testing and extra considerations that need to be made to merge code or resolve issues fully. **There is no TL;DR. Read this document based on the type of contribution you have.** Thanks in advance!
* [Code of Conduct (Issues and Merge Requests)]()
* [Design Decisions (Merge Requests)]()
  * [Suggestions That Will Make A Fool Out Of Yourself (Issues)]()
* [Style Guide(s) (Merge Requests)]()
* [Repository Information (Issues and Merge Requests)]()
  * [Labels]()
  
More will be added later.