# Pilot
A blazingly fast, extensible, and independent Minecraft server.
[Documentation](https://gitlab.com/AlexFischbacher/pilot-new/-/wikis/Home) · [Releases](https://gitlab.com/AlexFischbacher/pilot-new/-/releases) · [Website](https://pilotmc.net)

## Project Status
Pilot is currently in the conceptualization and design stage. As such, features may be changed or added to improve the project. Source code has not been writen yet, but feature requests are always accepted. To release Pilot past an alpha or beta stage (1.0.0), the following requirements must be fulfilled:
- [ ] Players can join and interact with the server.
- [ ] The protocol for 1.15 is fulfilled completely.
- [ ] All features present in the base game currently are implemented, and are working as intended ("as intended" doesn't necessarily mean bug-compatible).
- [ ] World generation represents that of Minecraft's defaults, allowing for expected behavior of world generation.
- [ ] The world format (Globe) is finalized and ready for stable use as the default in Pilot. 
- [ ] A web server for plugins is created and is started as a component of the main server.
- [ ] The plugin API is in a stable state, resulting in the finalization of plugin development features.

## What is Pilot?
Pilot is an independent Minecraft server implementation, made to solve issues related to extensibility, performance, portability, scalability, and user experience.

## Why Pilot?
* **Pilot adheres to the Minecraft protocol, but works hard to completely eliminate bugs related to gameplay.** Bugs suck. Fortunately, Pilot is designed around clean, reliable code that is heavily audited and improved bi-weekly.
* **Pilot is documented.** All code within the Pilot codebase is lovingly commented, and the wiki is extremely in-depth on developing for Pilot. Developers from Spigot or Sponge should have no issue diving right into Pilot plugin development.
* **Pilot is extremely configurable.** With the configuration, you can tweak gameplay to exactly how you want it to be. Configuration options for things like PvP systems and TNT fixes come built-in.
* **Pilot is free (in both senses of the word).** Pilot is licensed under the MPL 2.0 license, meaning that distributed versions of Pilot must disclose their source code and retain the MPL license with the files they use. The MPL is compatible with the GPL and is approved by the Free Software Foundation and the Open-Source Initiative.
* **Pilot is parallel.** Pilot purposefully splits different tasks into different threads to utilize the CPU in an efficient way. Never again deal with lag machines that affect the entire server's TPS! Individual threads that decrease TPS synchronize with the main server tickrate when stabilized.
* **Pilot is made for users.** Pilot prioritizes user experience over everything else. As such, Pilot comes with extra features that should help the life of a server administrator, such as in-depth diagnostics of the server that can be accessed with the API, file tracking via libgit, and a source-based in-game package manager. 

## Why...?
* **C?** C is one of the most well-known and portable compiled languages out there. Despite memory safety concerns brought up with the Rust language, C remains the one that has stood the test of time. Heavy audits of the codebase more than make up for the possible errors that can occur while using a language like C.
* **LibreSSL over OpenSSL?** LibreSSL is simply better in terms of development (`libtls` is only available with LibreSSL) and security. LibreSSL was made as a fork to OpenSSL by the OpenBSD team, who are known for being extra careful about security and code cleanliness.
* **a REST API?** By implementing a REST API for the scripting/plugin system, Pilot can support *any* programming language with HTTP(S) and JSON support. The plugin server that runs as a part of the main server allows for the loose coupling of plugins and the main server, turning them into their own standalone programs. I believe this is a better approach for compatibility and resource management/control of individual plugins.
* **TOML over YAML?** TOML is now in a relatively stable state, and I prefer it over YAML because it is not nearly as whitespace-sensitive. I would generally like to use [Dhall](https://dhall-lang.org/) in the future, but there are no stable methods of using it with C programs at this time, and there [may never be](https://github.com/dhall-lang/dhall-lang/issues/2).

## Why not Pilot?
* **Pilot is API-incompatible with Spigot.** Spigot (and any derivation from Bukkit) is viewed as harmful to the Pilot project, and as such API-compatibility with Spigot will not happen, ever. Spigot (and its code-base) is buggy, largely deprecated, slow, and tightly coupled. Spigot has abandoned the 1.8 version of Minecraft, despite it being extremely important for huge networks to thrive. Spigot plugin development forces developers to reinvent the wheel time and time again due to backwards compatibility. Despite this, there are more plugins on Spigot merely due to the length of time it has been around. *It is very likely that you will need to develop your own plugins at this stage if you are a server owner. It is very likely that you will need to completely remake your plugins from the ground up if you have developed Spigot plugins before.* Even so, we encourage you to look through (and contribute to!) the documentation surrounding the REST API that Pilot uses. We hope that you find joy in developing plugins for Pilot! :)
